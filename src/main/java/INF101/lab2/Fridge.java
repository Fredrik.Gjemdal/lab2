package INF101.lab2;
import java.util.ArrayList;
import java.util.NoSuchElementException;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.ArrayList;

public class Fridge implements IFridge {  

    int max = 20;
    ArrayList<FridgeItem> itemsFridge = new ArrayList<FridgeItem>();
    
    @Override
    public int nItemsInFridge() {
        int amount = itemsFridge.size();
        return amount;
    }

    @Override
    public int totalSize() {
        return max;
    }

    @Override
    public boolean placeIn(FridgeItem item) {
        if (nItemsInFridge() < totalSize()) {
            itemsFridge.add(item);
            return true;
        }
        return false;
    }

    @Override
    public void takeOut(FridgeItem item) {
        if (itemsFridge.contains(item)) {
            itemsFridge.remove(item);
            
        }
        else {
            throw new NoSuchElementException();
        }
        
    }

    @Override
    public void emptyFridge() {
        itemsFridge.clear();
        
    }

    @Override
    public List<FridgeItem> removeExpiredFood() {
        ArrayList<FridgeItem> expiredFood = new ArrayList<>();
        for (FridgeItem fridgeItem : itemsFridge) {
            if (fridgeItem.hasExpired()) {
                expiredFood.add(fridgeItem);
            }
        }
        for (FridgeItem expiredItem : expiredFood) {
            itemsFridge.remove(expiredItem);
        }
        return expiredFood;
    }

}
